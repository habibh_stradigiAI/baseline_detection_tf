from keras.layers import Dropout, Conv2D, Conv2DTranspose
from keras.layers import BatchNormalization, LeakyReLU 
from keras.layers import Input, Concatenate, ReLU, Lambda
from keras.models import Sequential, Model
import keras.backend as K 

class CGAN():
    """
    This class builds a Conditional GAN (CGAN). The Generator model is a U-Net which receives 
    a document image as input and delivers its detected baselines as a mask image. 
    The Discriminator is a fully convolutional network. 

    Examples
    --------
    cgan_builder = CGAN()
    # Build the generator
    nnG = cgan_builder.get_model_generator()
    # Build the discriminator
    nnD = cgan_builder.get_model_discriminator()

    Parameters
    ---------
    output_nc : integer, default 2 (binary). 
        Number of classes i.e. number of channels at the generator output. 
    ngf : integer, default 64
        Number of filters in the first convolutional block
    input_shape : array-like, default [1024, 768, 1]
        Shape of the input of the U-Net (generator)
    
    Attributes
    ----------
    get_model_generator 
        generates the Keras Model of the generator 
    get_model_discriminator
        generates the discriminator model
    get_model_combined
        combines the generator and discriminator to make the cGAN

    Reference
    ---------
    This model is a Keras implementation of the P2PaLA model proposed in the following paper:
    https://arxiv.org/pdf/1806.08852.pdf
    https://github.com/lquirosd/P2PaLA 
    Note that we modified the training and inference pipelines to improve its robustness. 
    """
    def __init__(self,
                 output_nc=2,
                 ngf=64,
                 input_shape=[1024, 768, 1]
        ):
        self.output_nc = output_nc
        self.ngf = ngf
        self.input_shape = input_shape

    def get_model_generator(self):

        """
        generates the Keras Model of the generator. 

        Examples
        --------
        cgan_builder = CGAN()
        nnG = cgan_builder.get_model_generator()

        Parameters
        ----------
        cnn_num_features : array like, default [16, 32, 48, 64, 80]
            A list of 5 integers determining the number of features in each convolutional layer.
        cnn_kernel_size : array like, default [(3,3), (3,3), (3,3), (3,3), (3,3)]
            A list of 5 tuples determining the size of kernels in each convolutional layer.
        cnn_strides : array like, default [(1,1), (1,1), (1,1), (1,1), (1,1)]
            A list of 5 tuples determining the strides in each convolutional layer.
        cnn_maxpool_size : array like, default [(2,2), (2,2), (2,2), (0,0), (0,0)]
            A list of 5 tuples determining the pool size in each convolutional layer.
        cnn_batch_norm : array like, default [True, True, True, True, True]
            A list of 5 booleans determining whether a batch norm layer is used in each convolutional layer.
        cnn_dropout : array like, default [0, 0, 0.2, 0.2, 0.2]
            A list of 5 scalars determining the dropout probability in each convolutional layer. If 0, no
            dropout will be applied.
        rnn_num_layers : int, default 5
            Number of BLSTM layers.
        rnn_num_units : int, default 256
            Number of hidden units in BLSTM layers.
        rnn_dropout : float, default 0.5
            The dropout probability applied before each of the BLSTM layers.
        linear_dropout : float, default 0.5
            The dropout probability applied before the linear (dense) layer.
        num_characters : int, default None
            Total number of unique characters in the training set (num classes).
        max_label_lens : int
            Maximum length of the ground-truth labels in the dataset.
        input_shape : array-like
            Dimension of the input images (not considering the batch size).
        input_lens : array-like
            Actual widths (before padding) of the input features after the convolutional layers are applied. 
        lbl_lens : array-like
            Actual lengths (before padding) of the ground-truth labels.
        
        Returns
        -------
        crnn_model : object
            The Keras model with 4 inputs and 2 outputs as follow:
            Inputs: input_image, y_true, input_length and label_length
            Outputs: loss_out (ctc_loss) and y_decode (ctc_decode)
        width_factor : int
            This factor is a scale by whitch the width of the input image is downsized by the 
            pooling layers. DataGenerator class needs this factor to calculate the length of the 
            features at the input of the BLSTM layer.
        """

        def _downsample(inner_nc, 
                        block_type='inner',
            ):

            model = Sequential()

            if block_type == 'center':
                model.add(LeakyReLU(alpha=0.2, trainable=True))
                model.add(Conv2D(filters=inner_nc, 
                                 kernel_size=4,
                                 strides=2, 
                                 padding='same',
                                 use_bias=False
                ))
            elif block_type == 'inner':
                model.add(LeakyReLU(alpha=0.2, trainable=True))
                model.add(Conv2D(filters=inner_nc, 
                                 kernel_size=4,
                                 strides=2, 
                                 padding='same',
                                 use_bias=False
                ))
                model.add(BatchNormalization(momentum=0.1, epsilon=1e-5))
            elif block_type == 'out':
                model.add(Conv2D(filters=inner_nc, 
                                 kernel_size=4,
                                 strides=2, 
                                 padding='same',
                                 use_bias=False
                ))

            return model
        
        def _upsample(output_nc, 
                      block_type='inner',
                      useDO=False
            ):

            model = Sequential()

            if block_type == 'center':
                model.add(ReLU(trainable=True))
                model.add(Conv2DTranspose(filters=output_nc,
                                          kernel_size=4,
                                          strides=2,
                                          padding='same',
                                          use_bias=False
                ))
                model.add(BatchNormalization(momentum=0.1, epsilon=1e-5))
                model.add(Dropout(rate=0.5))

            elif block_type == 'inner':
                model.add(ReLU(trainable=True))
                model.add(Conv2DTranspose(filters=output_nc,
                                         kernel_size=4,
                                         strides=2,
                                         padding='same',
                                         use_bias=False
                ))
                model.add(BatchNormalization(momentum=0.1, epsilon=1e-5))
                if useDO:
                    model.add(Dropout(rate=0.5))

            elif block_type == 'out':
                model.add(ReLU(trainable=True))
                model.add(Conv2DTranspose(filters=output_nc,
                                        kernel_size=4,
                                        strides=2,
                                        padding='same',
                                        use_bias=False,
                                        activation='softmax'
                ))

            return model

        # Input Placeholders
        d0 = Input(shape=self.input_shape, name='input_image_g')

        down_stack = [
            _downsample(inner_nc=self.ngf, block_type='out'),
            _downsample(inner_nc=self.ngf*2),
            _downsample(inner_nc=self.ngf*4),
            _downsample(inner_nc=self.ngf*8),
            _downsample(inner_nc=self.ngf*8),
            _downsample(inner_nc=self.ngf*8),
            _downsample(inner_nc=self.ngf*8),
            _downsample(inner_nc=self.ngf*8, block_type='center')
        ]

        up_stack = [
            _upsample(output_nc=self.ngf*8, block_type='center'),
            _upsample(output_nc=self.ngf*8, useDO=True),
            _upsample(output_nc=self.ngf*8, useDO=True),
            _upsample(output_nc=self.ngf*8),
            _upsample(output_nc=self.ngf*4),
            _upsample(output_nc=self.ngf*2),
            _upsample(output_nc=self.ngf)
        ]

        last = _upsample(output_nc=self.output_nc, block_type='out') 

        concat = Concatenate()

        skips = []

        cnt = 0
        for down in down_stack:
            if cnt == 0:
                x = down(d0)
            else:
                x = down(x)
            cnt += 1
            skips.append(x)

        skips = reversed(skips[:-1])

        for up, skip in zip(up_stack, skips):
            x = up(x)
            x = concat([x, skip])

        x = last(x)

        return Model(d0, x)

    def get_model_discriminator(self):
        """
        """
        # The following input is the input image x
        input_img_d = Input(shape=self.input_shape, name='input_img_d')
        # The following input could be either from nnG output or ground truth
        input_lbl_d = Input(shape=self.input_shape, name='input_lbl_d') 

        nnD_input = Concatenate(axis=-1)([input_img_d, input_lbl_d])

        d_out = Conv2D(filters=self.ngf, 
                       kernel_size=4, 
                       strides=2, 
                       padding='same', 
                       use_bias=False)(nnD_input)
        d_out = LeakyReLU(alpha=0.2, trainable=True)(d_out)

        d_out = Conv2D(filters=self.ngf*2, 
                       kernel_size=4, 
                       strides=2, 
                       padding='same', 
                       use_bias=False)(d_out)
        d_out = BatchNormalization(momentum=0.1, epsilon=1e-5)(d_out)
        d_out = LeakyReLU(alpha=0.2, trainable=True)(d_out)

        d_out = Conv2D(filters=self.ngf*4, 
                       kernel_size=4, 
                       strides=2, 
                       padding='same', 
                       use_bias=False)(d_out)
        d_out = BatchNormalization(momentum=0.1, epsilon=1e-5)(d_out)
        d_out = LeakyReLU(alpha=0.2, trainable=True)(d_out)

        d_out = Conv2D(filters=self.ngf*8, 
                       kernel_size=4, 
                       strides=1, 
                       padding='same', 
                       use_bias=False)(d_out)
        d_out = BatchNormalization(momentum=0.1, epsilon=1e-5)(d_out)
        d_out = LeakyReLU(alpha=0.2, trainable=True)(d_out)

        logits = Conv2D(filters=1, 
                        kernel_size=4, 
                        strides=1, 
                        padding='same', 
                        use_bias=False, 
                        activation='sigmoid')(d_out)

        return Model(inputs=[input_img_d, input_lbl_d], outputs=logits)

    def get_model_combined(self, nnG, nnD):

        # Input images
        img_in = Input(shape=self.input_shape)

        # Generate a fake version of img_cn
        nnG_out = nnG(img_in)
        # Apply argmax as discriminator receives 1-channel images
        nnG_out_argmax = Lambda(self.argmax_input)(nnG_out)

        # Discriminators determines validity of translated images / condition pairs
        nnD_out = nnD([img_in, nnG_out_argmax])

        # Create the combined model
        combined_model = Model(inputs=img_in, outputs=[nnG_out, nnD_out])

        return combined_model

    def argmax_input(self, input_fake):
        input_fake_argmax = K.cast(K.argmax(input_fake, axis=-1), dtype='float32')
        input_fake_argmax = K.expand_dims(input_fake_argmax, axis=-1) 
        return input_fake_argmax
