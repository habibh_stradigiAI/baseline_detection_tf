import os

import numpy as np
import time
import cv2
import datetime
import math
import glob
import shutil

from keras.optimizers import Adam
from keras.callbacks import TensorBoard
from keras.preprocessing.image import ImageDataGenerator

import tools.polyapprox as pa 
from tools.xmlPAGE import pageData
import tools.page2page_eval as p2peval

from model import CGAN

class BaselineDetector():

    """ 
    This class is used for training, validating and predicting the baseline detection model. 

    Examples
    --------
    detector = BaselineDetector()
    detector.fit(X, y)
    detector.partial_fit(X, y)
    all_baselines = detector.predict(X)

    Parameters
    ----------
    ngf : int, default 64
        The number of features in the first convolutional layer
    input_dimensions : array-like, default [1024, 768, 1]
        The input dimention of the model
    learning_rate : float, default 0.001
        The learning rate.
    early_stopping : int, default 100
        Stop the training after this number of epochs with no improvement in validation accuracy. 
    batch_size : int, default 16
        Batch size. 
    output_dir : str, default None
        The directory where the best models weights are saved during the training. This directory is also used to load the 
        model weights in partial_fit or inference. 
    logs_dir : str, default None
        The directory where the log is saved
    num_epochs : int, default 1000
        Total number of epochs for training
    loss_lambda : float, default 100
        The weight of combining the losses in GAN: gan_loss = (loss_lambda * g_loss) + shared_loss
    val_metric : str, default 'iou'
        The metric used in validation. It must be either 'iou' (faster) or 'f1_score' (more accurate)

    Attributes
    ----------
    fit 
        It recieves the inputs X and labels y and runs the training from scratch. It uses a validation set to evaluate the 
        accuracy using one of the metrics above. The training stops if the number of consecutive epochs with no improvement in validation 
        accuracy equals 'early_stopping'.
    partial_fit 
        It recieves the inputs X and labels y and continoues the training from the last best model saved in 'output_dir'. 
    predict 
        It recieves the inputs X and predicts the output text based on the best model saved in 'output_dir'. 
    """

    def __init__(self, 
                 ngf=64,
                 input_dimensions = [1024, 768, 1],
                 learning_rate=0.001,
                 early_stopping=100,
                 batch_size=8,
                 output_dir=None,
                 logs_dir=None,
                 num_epochs=1000,
                 loss_lambda=100,
                 val_metric='iou' # iou or f1_score
        ):

        self.ngf = ngf
        self.input_dimensions = input_dimensions
        self.learning_rate = learning_rate
        self.early_stopping = early_stopping
        self.batch_size = batch_size
        self.output_dir = output_dir
        self.logs_dir = logs_dir
        self.num_epochs = num_epochs
        self.loss_lambda = loss_lambda
        self.val_metric = val_metric

        # generator's optimizer
        self.optimizer_G = Adam(lr=self.learning_rate, beta_1=0.5)
        # discriminator's optimizer
        self.optimizer_D = Adam(lr=self.learning_rate, beta_1=0.5)

        # Default labels. The default is binary classification of baseline (1) and background (0)
        # Otherwise, it will change based on the training set in fit function
        self.classes_ = [0, 1]

    def fit(self, X, y):

        """ 
        It recieves the inputs X and labels y and runs the training from scratch. It uses a validation set to evaluate the 
        accuracy using one of the metrics above. The training stops if the number of consecutive epochs with no improvement in validation 
        accuracy equals 'early_stopping'.

        Examples
        --------
        detector = BaselineDetector()
        detector.fit(X, y)

        Parameters
        ----------
        X : array-like
            Contains two lists as: X = [train_imgs, validation_imgs] where:
            X[0] = is a list of all images in the training set (with variable dimentions)
            X[1] = is a list of all images in the validation set (with variable dimentions)
        y : list
            Contains two lists as: y = [train_labels, validation_labels] where:
            y[0] =  is a list of ground-truth masks in the training set (with the same dimentions as the model input).
            y[1] =  is a list of ground-truth masks in the validation set (with the same dimentions as the model input).

        Returns
        -------
        self : object
            The estimator itself. 
        """
        # Calculate number of unique classes in training set
        y_train = y[0]
        # Update class labels
        self.classes_ = np.unique(y_train)

        # Calculate the class weights to fix the class imbalance
        self.class_weight = {}
        prior = self._calculate_class_weight(y[0])
        for lbl in range(len(self.classes_)):
            self.class_weight[lbl] = prior[lbl]

        # Calculate the original size of the images before resizing to the model input dimensions
        # We don't need it in training to reconstruct the image to its original shape
        # We do this here because we need the original shape of the images for the inference
        X[0], org_shapes_train = self._resize_inputs(X[0])
        X[1], org_shapes_valid = self._resize_inputs(X[1])
        
        # data augmentation arguments
        d_type = 'float32'
        rotation_range = 10
        shear_range = 10
        zoom_range = 0.2

        # dictionary for image generator
        batch_gen_args_img = dict(dtype=d_type,
                                  rotation_range=rotation_range,
                                  shear_range=shear_range,
                                  zoom_range=zoom_range,
                            )
        # dictionary for label generator
        batch_gen_args_lbl = dict(dtype=d_type,
                                  rotation_range=rotation_range,
                                  shear_range=shear_range,
                                  zoom_range=zoom_range,
                            )

        train_generator = self._build_generator(batch_gen_args_img, 
                                                batch_gen_args_lbl, 
                                                X[0], y[0], org_shapes_train,
                                                self.batch_size, 
                                                shuffle=True
                                                )
        # Do the same for validation sets but without augmentation
        batch_gen_args_val = dict(dtype=d_type)
        valid_generator = self._build_generator(batch_gen_args_val, 
                                                batch_gen_args_val, 
                                                X[1], y[1], org_shapes_valid,
                                                self.batch_size, 
                                                shuffle=False
                                                )

        # Create the CGAN model
        cgan_builder = CGAN(output_nc=len(self.classes_),
                            ngf=self.ngf,
                            input_shape=self.input_dimensions
        )

        # Build and compile the discriminator
        nnD = cgan_builder.get_model_discriminator()
        nnD.summary()
        nnD.compile(optimizer=self.optimizer_D, 
                    loss='binary_crossentropy'
        )      

        #-------------------------
        # Construct Computational
        #   Graph of Generator
        #-------------------------
        # Build the Generator model
        nnG = cgan_builder.get_model_generator()
        nnG.summary()

        # Build and compile the combined model
        combined_model = cgan_builder.get_model_combined(nnG, nnD)
        # For the combined model we will only train the generator
        # So freez the discriminator
        combined_model.layers[-1].trainable = False
        combined_model.summary()
        combined_model.compile(optimizer=self.optimizer_G,
                               loss=['sparse_categorical_crossentropy', 'binary_crossentropy'],
                               loss_weights=[self.loss_lambda, 1] 
                               )     
    
        #--------------
        # Training Loop
        #--------------
        tb_clbck = TensorBoard(log_dir=self.logs_dir)
        tb_clbck.set_model(combined_model)
        
        early_stop = 0
        epoch = -1
        epoch_metric_val_max = 0

        # Calculate the shape of discriminator's output
        d_out_shape = nnD.layers[-1].output_shape

        if self.val_metric == 'f1_score':
            # Create a tmp folder for saving the xml files
            dir_tmp = os.path.join(self.output_dir, 'tmp_val')
            dir_gen = os.path.join(dir_tmp, 'page_gen')
            dir_gt  = os.path.join(dir_tmp, 'page_gt')

            if os.path.isdir(dir_tmp):
                shutil.rmtree(dir_tmp, ignore_errors=True)
                os.mkdir(dir_tmp)
                os.mkdir(dir_gen)
                os.mkdir(dir_gt)
            else:
                os.mkdir(dir_tmp)
                os.mkdir(dir_gen)
                os.mkdir(dir_gt)

        while early_stop < self.early_stopping and epoch < self.num_epochs:

            epoch = epoch + 1
            epoch_lossG = 0
            epoch_lossGAN = 0
            epoch_lossR = 0
            epoch_lossD = 0
            epoch_iou_val = 0

            # -------------
            # Training Loop
            # -------------
            # Iterate over all the batches in training set.
            for batch_indx_train, ((x, org_size), y_gt) in enumerate(train_generator):
                start_time = datetime.datetime.now()
                # ---------------------
                #  Train Discriminator
                # ---------------------
                # Because the last batch of the epoch may have different size
                gen_batch_size = y_gt.shape[0]

                # Adversarial ground truths for discriminator
                real_y_d = np.ones((gen_batch_size, d_out_shape[1], d_out_shape[2], d_out_shape[3]))
                fake_y_d = np.zeros((gen_batch_size, d_out_shape[1], d_out_shape[2], d_out_shape[3]))
                
                y_gen = nnG.predict_on_batch(x)
                y_gen_argmax = np.expand_dims(np.argmax(y_gen, axis=-1), axis=-1)

                # Train discriminator every other batches as it learns faster than genersator
                if batch_indx_train % 2 == 0:
                    d_loss_real = nnD.train_on_batch([x, y_gt], real_y_d)
                    d_loss_fake = nnD.train_on_batch([x, y_gen_argmax], fake_y_d)
                    d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)
                # -----------------
                #  Train Generator
                # -----------------
                g_loss = combined_model.train_on_batch(x, [y_gt, real_y_d], class_weight=self.class_weight)

                # Plot the progress
                elapsed_time = datetime.datetime.now() - start_time
                metrics_batch = {'gan_loss': g_loss[0], # (lambda * g_loss) + shared_loss
                                 'g_loss'  : g_loss[1],
                                 'd_loss'  : d_loss,
                                 'shared_loss': g_loss[2]}

                epoch_lossG = epoch_lossG + (metrics_batch['g_loss']) # / gen_batch_size)
                epoch_lossGAN = epoch_lossGAN + (metrics_batch['gan_loss']) # / gen_batch_size)
                epoch_lossR = epoch_lossR + metrics_batch['shared_loss'] 
                epoch_lossD = epoch_lossD + metrics_batch['d_loss'] 

                print ("[Epoch %d/%d] [Batch %d/%d] [D loss: %f] [G loss: %f] time: %s" % (epoch, self.num_epochs,
                                                                        batch_indx_train, math.ceil(len(X[0]) / self.batch_size) - 1,
                                                                        metrics_batch['d_loss'],
                                                                        metrics_batch['g_loss'],
                                                                        elapsed_time))
                if batch_indx_train + 1 >= math.ceil(len(X[0]) / self.batch_size):
                    break
                    
            # ---------------
            # Validation Loop
            # ---------------
            # Iterate over all the batches in validation set.
            for batch_indx_val, ((x, org_size), y_gt) in enumerate(valid_generator):

                y_gen_valid = nnG.predict_on_batch(x)
                
                if self.val_metric == 'iou':
                    # Calculate the batch IoU of Val set
                    batch_iou_val = self._iou_coef_np(y_gt, y_gen_valid)
                    epoch_iou_val = epoch_iou_val + batch_iou_val

                elif self.val_metric == 'f1_score':
                    # Calculate the F1 Score based on Transkribus metric
                    for i in range(x.shape[0]):
                        o_img = np.squeeze(0.5*255*(x[i] + 1), axis=-1)
                        gt_mask = np.squeeze(y_gt[i], axis=-1)
                        y_argmax = np.argmax(y_gen_valid[i], axis=-1)

                        file_name_gen = os.path.join(dir_gen, str(batch_indx_val) + str(i) + '.xml')
                        file_name_gt  = os.path.join(dir_gt, str(batch_indx_val) + str(i) + '.xml')

                        baselines_gen = self._gen_page(o_img, y_argmax, out_dir=file_name_gen)

                        # Generate the baseline of the ground truth mask only once
                        if epoch == 0:
                            baselines_gt  = self._gen_page(o_img, gt_mask, out_dir=file_name_gt)
                    
                if batch_indx_val + 1 >= math.ceil(len(X[1]) / self.batch_size):
                    break

            # ----------------------------
            # Save and monitor the results
            # ----------------------------
            if self.val_metric == 'iou':
                epoch_metric_val = epoch_iou_val / (batch_indx_val + 1)

            elif self.val_metric == 'f1_score':
                hyp_xml_list = self._xml_list_generator(dir_gen)
                gt_xml_list = self._xml_list_generator(dir_gt)
                presicion_val, recall_val, F1_score_val = p2peval.compute_metrics(hyp_xml_list, gt_xml_list) 
                epoch_metric_val = F1_score_val + 0

            metrics_epoch = {'gan_loss': epoch_lossGAN, # / (batch_indx_train + 1), # (lambda * g_loss) + d_loss
                             'g_loss'  : epoch_lossG, # / (batch_indx_train + 1),
                             'd_loss'  : epoch_lossD, # / (batch_indx_train + 1),
                             'shared_loss': epoch_lossR, # / (batch_indx_train + 1),
                             'epoch_metric_val' : epoch_metric_val
                             } # 
            tb_clbck.on_epoch_end(epoch, metrics_epoch)

            # Save the generator model weights if epoch_iou_val increases
            if metrics_epoch['epoch_metric_val'] > epoch_metric_val_max:
                early_stop = 0
                # Update the epoch_iou_val_max
                epoch_metric_val_max = metrics_epoch['epoch_metric_val'] + 0
                # Save the generator
                nnG.save_weights(os.path.join(self.output_dir, 'nnG.h5'))
                # # Save the discriminator
                # nnD.save_weights(os.path.join(self.output_dir, 'nnD.h5'))

                print('New GAN saved as ' + str(self.val_metric) + ' of validation increased to: ', epoch_metric_val_max)

                # Write the result in a txt file
                with open(os.path.join(self.output_dir, 'nnG_metric_val.txt'), 'w') as txt_iou:
                    txt_iou.write('The ' + str(self.val_metric) + ' of validation is:\n')
                    txt_iou.write(str(round(epoch_metric_val_max, 4)) + '\n\n')
                    txt_iou.write('Epoch:\n')
                    txt_iou.write(str(epoch))

            else:
                early_stop = early_stop + 1
                print('The ' + str(self.val_metric) + ' of validation NOT increased. Early stop increased to: %d/%d' % (early_stop, self.early_stopping))
        
        if self.val_metric == 'f1_score':
            shutil.rmtree(dir_tmp, ignore_errors=True)
            
        return self

    def partial_fit(self, X, y):
        """ 
        Thit function recieves the inputs X and labels y and continoues the training from the last model saved
        in the output_dir. It uses a validation set to evaluate accuracy. The training stops 
        if the number of consecutive epochs with no improvement in validation accuracy equals 'early_stopping'.

        Examples
        --------
        detector = BaselineDetector()
        detector.partial_fit(X, y)

        Parameters
        ----------
        X : array-like
            Contains two lists as: X = [train_imgs, validation_imgs] where:
            X[0] = is a list of all images in the training set (with variable dimentions)
            X[1] = is a list of all images in the validation set (with variable dimentions)
        y : list
            Contains two lists as: y = [train_labels, validation_labels] where:
            y[0] =  is a list of ground-truth masks in the training set (with the same dimentions as the model input).
            y[1] =  is a list of ground-truth masks in the validation set (with the same dimentions as the model input).

        Returns
        -------
        self : object
            The estimator itself. 
        """

        # Calculate the original size of the images before resizing to the model input dimensions
        # We don't need it in training to reconstruct the image to its original shape
        # We do this here because we need the original shape of the images for the inference
        X[0], org_shapes_train = self._resize_inputs(X[0])
        X[1], org_shapes_valid = self._resize_inputs(X[1])
        
        # arguments for data augmentation
        d_type = 'float32'
        rotation_range = 10
        shear_range = 10
        zoom_range = 0.2

        batch_gen_args_img = dict(dtype=d_type,
                                  rotation_range=rotation_range,
                                  shear_range=shear_range,
                                  zoom_range=zoom_range,
                            )
        batch_gen_args_lbl = dict(dtype=d_type,
                                  rotation_range=rotation_range,
                                  shear_range=shear_range,
                                  zoom_range=zoom_range,
                            )

        train_generator = self._build_generator(batch_gen_args_img, 
                                                batch_gen_args_lbl, 
                                                X[0], y[0], org_shapes_train,
                                                self.batch_size, 
                                                shuffle=True
                                                )
        # Do the same for validation set but without augmentation
        batch_gen_args_val = dict(dtype=d_type)
        valid_generator = self._build_generator(batch_gen_args_val, 
                                                batch_gen_args_val, 
                                                X[1], y[1], org_shapes_valid,
                                                self.batch_size, 
                                                shuffle=False
                                                )

        # Create the CGAN model
        cgan_builder = CGAN(output_nc=len(self.classes_),
                            ngf=self.ngf,
                            input_shape=self.input_dimensions
                        )

        # Build and compile the discriminator
        nnD = cgan_builder.get_model_discriminator()
        nnD.summary()
        
        #-------------------------
        # Construct Computational
        #   Graph of Generator
        #-------------------------
        # Build the Generator model
        nnG = cgan_builder.get_model_generator()
        nnG.summary()

        print('Loading the GAN weights from the best saved model...')
        nnG.load_weights(os.path.join(self.output_dir, 'nnG.h5'))
        nnD.load_weights(os.path.join(self.output_dir, 'nnD.h5'))
        print('Loading finished successfully')

        nnD.compile(optimizer=self.optimizer_D, 
                    loss='binary_crossentropy'
        )      

        combined_model = cgan_builder.get_model_combined(nnG, nnD)
        # For the combined model we will only train the generator
        # So freez the discriminator
        combined_model.layers[-1].trainable = False
        combined_model.summary()
        combined_model.compile(optimizer=self.optimizer_G,
                               loss=['sparse_categorical_crossentropy', 'binary_crossentropy'],
                               loss_weights=[self.loss_lambda, 1] # CHANGED SO THAT NO GAN IS USED
                               )     
        #--------------
        # Training Loop
        #--------------
        tb_clbck = TensorBoard(log_dir=self.logs_dir)
        tb_clbck.set_model(combined_model)
        
        early_stop = 0
        epoch = -1
        epoch_metric_val_max = 0

        # Adversarial ground truths for discriminator
        d_out_shape = nnD.layers[-1].output_shape

        if self.val_metric == 'f1_score':
            # Create a tmp folder for saving the xml files
            dir_tmp = os.path.join(self.output_dir, 'tmp_val')
            dir_gen = os.path.join(dir_tmp, 'page_gen')
            dir_gt  = os.path.join(dir_tmp, 'page_gt')

            if os.path.isdir(dir_tmp):
                shutil.rmtree(dir_tmp, ignore_errors=True)
                os.mkdir(dir_tmp)
                os.mkdir(dir_gen)
                os.mkdir(dir_gt)
            else:
                os.mkdir(dir_tmp)
                os.mkdir(dir_gen)
                os.mkdir(dir_gt)

        while early_stop < self.early_stopping and epoch < self.num_epochs:

            epoch = epoch + 1
            epoch_lossG = 0
            epoch_lossGAN = 0
            epoch_lossR = 0
            epoch_lossD = 0
            epoch_iou_val = 0

            # -------------
            # Training Loop
            # -------------
            # Iterate over all the batches in training set.
            for batch_indx_train, ((x, org_size), y_gt) in enumerate(train_generator):
                start_time = datetime.datetime.now()
                # ---------------------
                #  Train Discriminator
                # ---------------------
                # Because the last batch of the epoch may have different size
                gen_batch_size = y_gt.shape[0]

                real_y_d = np.ones((gen_batch_size, d_out_shape[1], d_out_shape[2], d_out_shape[3]))
                fake_y_d = np.zeros((gen_batch_size, d_out_shape[1], d_out_shape[2], d_out_shape[3]))
                
                y_gen = nnG.predict_on_batch(x)
                y_gen_argmax = np.expand_dims(np.argmax(y_gen, axis=-1), axis=-1)

                # Train discriminator every other batches
                if batch_indx_train % 2 == 0:
                    d_loss_real = nnD.train_on_batch([x, y_gt], real_y_d)
                    d_loss_fake = nnD.train_on_batch([x, y_gen_argmax], fake_y_d)
                    d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)
                # -----------------
                #  Train Generator
                # -----------------
                g_loss = combined_model.train_on_batch(x, [y_gt, real_y_d], class_weight=self.class_weight)

                # Plot the progress
                elapsed_time = datetime.datetime.now() - start_time
                metrics_batch = {'gan_loss': g_loss[0], # (lambda * g_loss) + shared_loss
                                 'g_loss'  : g_loss[1],
                                 'd_loss'  : d_loss,
                                 'shared_loss': g_loss[2]}

                epoch_lossG = epoch_lossG + (metrics_batch['g_loss']) # / gen_batch_size)
                epoch_lossGAN = epoch_lossGAN + (metrics_batch['gan_loss']) # / gen_batch_size)
                epoch_lossR = epoch_lossR + metrics_batch['shared_loss'] 
                epoch_lossD = epoch_lossD + metrics_batch['d_loss'] 

                print ("[Epoch %d/%d] [Batch %d/%d] [D loss: %f] [G loss: %f] time: %s" % (epoch, self.num_epochs,
                                                                        batch_indx_train, math.ceil(len(X[0]) / self.batch_size) - 1,
                                                                        metrics_batch['d_loss'],
                                                                        metrics_batch['g_loss'],
                                                                        elapsed_time))
                if batch_indx_train + 1 >= math.ceil(len(X[0]) / self.batch_size):
                    break
                    
            # ---------------
            # Validation Loop
            # ---------------
            # Iterate over all the batches in validation set.
            for batch_indx_val, ((x, org_size), y_gt) in enumerate(valid_generator):

                y_gen_valid = nnG.predict_on_batch(x)
                
                if self.val_metric == 'iou':
                    # Calculate the batch IoU of Val set
                    batch_iou_val = self._iou_coef_np(y_gt, y_gen_valid)
                    epoch_iou_val = epoch_iou_val + batch_iou_val

                elif self.val_metric == 'f1_score':
                    # Calculate the F1 Score based on Transkribus metric
                    for i in range(x.shape[0]):
                        o_img = np.squeeze(0.5*255*(x[i] + 1), axis=-1)
                        gt_mask = np.squeeze(y_gt[i], axis=-1)
                        y_argmax = np.argmax(y_gen_valid[i], axis=-1)

                        file_name_gen = os.path.join(dir_gen, str(batch_indx_val) + str(i) + '.xml')
                        file_name_gt  = os.path.join(dir_gt, str(batch_indx_val) + str(i) + '.xml')

                        baselines_gen = self._gen_page(o_img, y_argmax, out_dir=file_name_gen)

                        # Generate the baseline of the ground truth mask only once
                        if epoch == 0:
                            baselines_gt  = self._gen_page(o_img, gt_mask, out_dir=file_name_gt)
                    
                if batch_indx_val + 1 >= math.ceil(len(X[1]) / self.batch_size):
                    break

            # ----------------------------
            # Save and monitor the results
            # ----------------------------
            if self.val_metric == 'iou':
                epoch_metric_val = epoch_iou_val / (batch_indx_val + 1)

            elif self.val_metric == 'f1_score':
                hyp_xml_list = self._xml_list_generator(dir_gen)
                gt_xml_list = self._xml_list_generator(dir_gt)
                presicion_val, recall_val, F1_score_val = p2peval.compute_metrics(hyp_xml_list, gt_xml_list) 
                epoch_metric_val = F1_score_val + 0

            metrics_epoch = {'gan_loss': epoch_lossGAN, # / (batch_indx_train + 1), # (lambda * g_loss) + d_loss
                             'g_loss'  : epoch_lossG, # / (batch_indx_train + 1),
                             'd_loss'  : epoch_lossD, # / (batch_indx_train + 1),
                             'shared_loss': epoch_lossR, # / (batch_indx_train + 1),
                             'epoch_metric_val' : epoch_metric_val
                             } # 
            tb_clbck.on_epoch_end(epoch, metrics_epoch)

            # Save the generator model weights if epoch_iou_val increases
            if metrics_epoch['epoch_metric_val'] > epoch_metric_val_max:
                early_stop = 0
                # Update the epoch_iou_val_max
                epoch_metric_val_max = metrics_epoch['epoch_metric_val'] + 0
                # Save the generator
                nnG.save_weights(os.path.join(self.output_dir, 'nnG.h5'))
                print('New Generator saved as ' + str(self.val_metric) + ' of validation increased to: ', epoch_metric_val_max)

                # Write the result in a txt file
                with open(os.path.join(self.output_dir, 'nnG_metric_val.txt'), 'w') as txt_iou:
                    txt_iou.write('The ' + str(self.val_metric) + ' of validation is:\n')
                    txt_iou.write(str(round(epoch_metric_val_max, 4)) + '\n\n')
                    txt_iou.write('Epoch:\n')
                    txt_iou.write(str(epoch))

            else:
                early_stop = early_stop + 1
                print('The ' + str(self.val_metric) + ' of validation NOT increased. Early stop increased to: %d/%d' % (early_stop, self.early_stopping))
        
        if self.val_metric == 'f1_score':
            shutil.rmtree(dir_tmp, ignore_errors=True)

        return self

    def predict(self, X, multi_scale_inference=True):
        """ 
        Thit function recieves the inputs X and predicts the output text using the best model saved
        in the output_dir. It also uses a multi-scale inference for improving the robustness to the 
        different font sizes in the same page. 

        Examples
        --------
        transcriptor = Transcriptor()
        transcriptor.predict(X)

        Parameters
        ----------
        X : list
            A list of all images in the inference set (with the same height but variable widths)
        multi_scale_inference : bool, default True
            If True, it uses a multi-scale inference and average the results for improving the robustness. 
            It is better to use this for documents with large font sizes especially those taken by camera
            or phones / tablets. 
            If False, it uses the original scale for inference. It is recommended for scanned documents

        Returns
        -------
        all_baselines : list
            A list of all baseline coordinates in the input documents. By default it gives 4 coordinates
            per baseline. 
        """

        # since it is inference
        y = []

        # Resize the images to the input dimension of the U-Net
        # We do this here because we need the original shape of the images for the inference
        X, org_shapes = self._resize_inputs(X)

        # If multi-scale inference is used, the batch size must be 1
        if multi_scale_inference:
            self.batch_size = 1
        else: 
            self.batch_size = 1 # to be changed later but it is recommended to use 1

        batch_gen_args_infer = dict(dtype='float32')
        test_generator  = self._build_generator(batch_gen_args_infer, 
                                                batch_gen_args_infer, 
                                                X, y, org_shapes,
                                                batch_size=self.batch_size, shuffle=False
                                )

        # Create the CGAN model
        cgan_builder = CGAN(output_nc=len(self.classes_),
                            ngf=self.ngf,
                            input_shape=self.input_dimensions
                        )

        # Build the Generator model
        nnG = cgan_builder.get_model_generator()
        nnG.summary()
        print('Loading the U-Net weights from the best saved model...')
        nnG.load_weights(os.path.join(self.output_dir, 'nnG.h5'))
        print('Loading finished successfully')

        # Threshold for classification of the multi-scale output
        TH_line_detect = 0.5
        # The list which saves all the baselines for all the input images 
        all_baselines = [] 

        for b_id, (x, org_size) in enumerate(test_generator):

            # create an array for saving different scale results for averaging
            merged_fonts = np.zeros((org_size[0][0], org_size[0][1]), dtype=float)

            # Create the multi-scale sub-batch for every input image
            if multi_scale_inference:
                x, scales = self._create_multi_scale_batch(x)
            else:
                scales = [1]

            y_gen = nnG.predict_on_batch(x)
            
            for i in range(len(x)):
                org_img = np.squeeze(0.5*255*(x[i] + 1), axis=-1)
                gen_mask = y_gen[i][:, :, 1]

                # Calculate the scale of resizing
                o_rows, o_cols = org_img.shape
                cScale = np.array([o_cols/self.input_dimensions[1],
                                   o_rows/self.input_dimensions[0]])

                # Crop and Resize the generated image to the original size of the image
                h_crp = int(scales[i]*org_img.shape[0])
                w_crp = int(scales[i]*org_img.shape[1])

                org_img = org_img[0:h_crp, 0:w_crp]
                gen_mask = gen_mask[0:h_crp, 0:w_crp]

                org_img = cv2.resize(org_img, (org_size[0][1], org_size[0][0]), interpolation=cv2.INTER_CUBIC)
                gen_mask = cv2.resize(gen_mask, (org_size[0][1], org_size[0][0]), interpolation=cv2.INTER_CUBIC)

                merged_fonts = np.add(merged_fonts, gen_mask)

                if scales[i] == 1:
                    org_img_full_scale = 1 * org_img

            merged_fonts = merged_fonts / len(scales)
            merged_fonts = np.where(merged_fonts >= TH_line_detect, 1, 0)

            # Calculate the baselines using the generated mask                   
            baselines = self._gen_page(org_img_full_scale, merged_fonts, cScale=cScale)

            all_baselines.append(baselines)

            if b_id + 1 >= math.ceil(len(X) / self.batch_size):
                break

        return all_baselines

    def _iou_coef_np(self, y_true, y_pred):
        """Count iou coefficient for output and ground-truth image using numpy."""
        num_labels = y_pred.shape[-1]

        y_true = np.squeeze(y_true, axis=-1)
        y_pred = np.argmax(y_pred, axis=-1)

        total_iou = 0.0
        for label in range(1, num_labels): # We do not calculate iou for label = 0 (background)
            total_iou = total_iou + self._iou_np(y_true, y_pred, label)
        return total_iou / (num_labels - 1)
    
    def _iou_np(self, y_true, y_pred, label):
        """
        Return the batch Intersection over Union (IoU) for a given label.
        Args:
            y_true: the expected y values as a one-hot
            y_pred: the predicted y values as a one-hot or softmax output
            label: the label to return the IoU for
        Returns:
            the batch IoU for the given label
        """
        # extract the label values using the argmax operator then
        # calculate equality of the predictions and truths to the label

        SMOOTH = 1e-9

        y_true_lbl = np.zeros_like(y_true)
        y_true_lbl[y_true==label] = 1
        y_pred_lbl = np.zeros_like(y_pred)
        y_pred_lbl[y_pred==label] = 1

        # calculate the |intersection| (AND) of the labels bor all images in batch
        intersection = np.sum(np.multiply(y_true_lbl, y_pred_lbl), axis=(1,2))
        # calculate the |union| (OR) of the labels bor all images in batch
        union = np.sum(y_true_lbl, axis=(1,2)) + np.sum(y_pred_lbl, axis=(1,2)) - intersection

        # avoid divide by zero - if the union is zero, return 1
        # otherwise, return the intersection over union
        iou = np.divide((intersection + SMOOTH), (union + SMOOTH))  # We smooth our devision to avoid 0/0

        # Calculate the mean of IoU over the batch
        iou_mean_batch = np.mean(iou)
        
        return iou_mean_batch

    def _resize_inputs(self, inputs):
        """
        This function is used for resizing the input images into the
        input dimensions of the model. It also returns the original
        dimensions of the images in another array of the same length
        as the number of images
        """
        outputs = []
        org_shapes = []

        for img in inputs:
            org_shape = img.shape
            img = cv2.resize(img, (self.input_dimensions[1], self.input_dimensions[0]), interpolation=cv2.INTER_CUBIC)

            # Because cv2 will squeeze the input dimension
            if org_shape[-1] == 1:
                img = np.expand_dims(img, axis=-1)

            outputs.append(img)
            org_shapes.append(org_shape)

        return np.array(outputs), np.array(org_shapes)


    def _build_generator(self, batch_gen_args_img, 
                         batch_gen_args_lbl, 
                         X, y, org_shapes, 
                         batch_size, 
                         shuffle):
        """
        This function is used for data generation
        """

        def _normalize_img(img):
            """
            image normalization
            """
            return (2 * img / 255.0) - 1
        def _normalize_gt(img):
            """
            ground truth mask normalization
            """
            return img / 255.0

        # Create the data generator for training
        # Provide the same seed and keyword arguments to the fit and flow methods
        seed = 1
        batch_gen_img = ImageDataGenerator(**batch_gen_args_img, preprocessing_function=_normalize_img)
        batch_gen_img.fit(X, augment=False, seed=seed)
        train_image_generator = batch_gen_img.flow((X, org_shapes),
                                                  batch_size=batch_size, 
                                                  shuffle=shuffle, 
                                                  seed=seed
                                )

        # Check if this is training or inference
        if len(y) > 0:
            # It is training
            batch_gen_msk = ImageDataGenerator(**batch_gen_args_lbl, preprocessing_function=_normalize_gt, fill_mode='constant', cval=0.0)
            batch_gen_msk.fit(y, augment=False, seed=seed)
            train_mask_generator = batch_gen_msk.flow(y, 
                                                      batch_size=batch_size, 
                                                      shuffle=shuffle, 
                                                      seed=seed
                                   )
            train_generator = zip(train_image_generator, train_mask_generator)
            return train_generator

        else:
            # It is inference
            return train_image_generator

    def _calculate_class_weight(self, y):
        """
        This function calculates the class weights for 
        fixing the class imbalances
        """
        w = np.ones(len(self.classes_), dtype=np.float)
        for label in y:
            label_normalized = np.array(label / 255.0, dtype='int64')
            w += np.bincount(label_normalized.flatten(), minlength=2)
        w = w / ((y.shape[0] * (y.shape[1]+y.shape[2])) + 2)
        w = 1 / np.log(1.02 + w)

        return w

    def _create_multi_scale_batch(self, x, scales=[1, 0.75, 0.5, 0.33, 0.25]):
        """
        This function generates a multi-scale batch for each image in the inference.
        In order to have the same dimensions, it pads the smaller scale image to their right
        and bottom. 
        """
        v_shape = x.shape[1]
        h_shape = x.shape[2]

        x_255 = 0.5 * 255 * (x[0] + 1)

        xx = []
        for scl in scales:
            x_new = cv2.resize(x_255, (int(scl*h_shape), int(scl*v_shape)), interpolation=cv2.INTER_CUBIC)

            bordersize_bottom = v_shape - int(scl*v_shape)
            bordersize_right  = h_shape - int(scl*h_shape)
            x_new = cv2.copyMakeBorder(x_new, 
                                       top=0, 
                                       bottom=bordersize_bottom, 
                                       left=0, 
                                       right=bordersize_right, 
                                       borderType=cv2.BORDER_CONSTANT, 
                                       value=255 #[mean, mean, mean]
            )

            x_new = np.expand_dims(2.0 * (x_new / 255.0) - 1, axis=-1)

            xx.append(x_new)

        return np.array(xx), scales


    def _gen_page(self,
                  o_img0, 
                  y_argmax, 
                  out_dir=None,
                  cScale=np.array([1, 1])
                  ):
        """
        This function is used to generate the PAGE xml files and claculate the 
        baseline coordinates from the detected masks. It uses the opencv tool
        for analyzing the binary regions in the detected masks. 
        """
        #--- sym link to original image 
        #--- TODO: check if orig image exist
        o_img = o_img0.astype(np.uint8) 
        o_rows, o_cols = o_img.shape
        if out_dir != None:
            page = pageData(out_dir)
            img_name = os.path.basename(out_dir)
            page.new_page(img_name, str(o_rows), str(o_cols)) 
        lines = y_argmax.astype(np.uint8) 
        reg_list= ['full_page']
        colors = {'full_page':0}
        r_data = np.zeros(lines.shape, dtype=np.uint8)
            
        reg_mask = np.zeros(r_data.shape,dtype='uint8')
        lin_mask = np.zeros(lines.shape,dtype='uint8')
        r_id = 0
        kernel = np.ones((5,5),np.uint8)

        #--- get regions and lines for each class
        for reg in reg_list:
            r_color = colors[reg]
            #--- fill the array is faster then create a new one or mult by 0
            reg_mask.fill(0)
            reg_mask[r_data == r_color] = 1
            contours, hierarchy = cv2.findContours(reg_mask,
                                                   cv2.RETR_EXTERNAL,
                                                   cv2.CHAIN_APPROX_SIMPLE)
            BASELINES = []
            for cnt in contours:
                #--- remove small objects
                if(cnt.shape[0] < 4):
                    continue
                if(cv2.contourArea(cnt) < 0.01 * self.input_dimensions[0]):
                    continue
                #--- get lines inside the region
                lin_mask.fill(0)
                rect = cv2.minAreaRect(cnt)
                #--- soft a bit the region to prevent spikes 
                epsilon = 0.005 * cv2.arcLength(cnt, True)
                approx = cv2.approxPolyDP(cnt, epsilon, True)
                r_id = r_id + 1
                approx= (approx * cScale).astype('int32')
                reg_coords = ''
                for x in approx.reshape(-1,2):
                    reg_coords = reg_coords + " {},{}".format(x[0],x[1])

                cv2.fillConvexPoly(lin_mask,points=cnt, color=(1,1,1))
                lin_mask = cv2.erode(lin_mask,kernel,iterations = 1)
                lin_mask = cv2.dilate(lin_mask,kernel,iterations = 1)
                reg_lines = lines * lin_mask
                #--- search for the lines

                l_cont, l_hier = cv2.findContours(reg_lines,
                                                cv2.RETR_EXTERNAL,
                                                cv2.CHAIN_APPROX_SIMPLE)
                if (len(l_cont) == 0):
                    continue
                if out_dir != None:
                    #--- Add region to XML only if there is some line
                    text_reg = page.add_element('TextRegion',
                                            str(r_id),
                                            reg,
                                            reg_coords.strip())
                n_lines = 0
                for l_id,l_cnt in enumerate(l_cont):
                    if (l_cnt.shape[0] < 4):
                        continue
                    if (cv2.contourArea(l_cnt) < 0.01 * self.input_dimensions[0]):
                        continue
                    #--- convert to convexHull if poly is not convex
                    if (not cv2.isContourConvex(l_cnt)):
                        l_cnt = cv2.convexHull(l_cnt)
                    lin_coords = ''
                    l_cnt = (l_cnt*cScale).astype('int32')
                    for l_x in l_cnt.reshape(-1,2): 
                        lin_coords = lin_coords + " {},{}".format(l_x[0],l_x[1])
                    (is_line, approx_lin) = self._get_baseline(o_img, l_cnt)
                    if is_line == False:
                        continue
                    if out_dir != None:
                        text_line = page.add_element('TextLine',
                                                    str(l_id) + '_' + str(r_id),
                                                    reg,
                                                    lin_coords.strip(),
                                                    parent=text_reg)
                    baseline = pa.points_to_str(approx_lin)
                    if out_dir != None:
                        page.add_baseline(baseline, text_line)
                    BASELINES.append(approx_lin)
                    n_lines += 1
                
                #--- remove regions without text lines
                if n_lines == 0 and out_dir != None:
                    page.remove_element(text_reg)

        if out_dir != None:    
            page.save_xml()
        return BASELINES

    def _get_baseline(self, Oimg, 
                      Lpoly, 
                      max_vertex=30, 
                      num_segments=4, 
                      approx_alg='optimal'
                      ):
        """
        """
        #--- Oimg = image to find the line
        #--- Lpoly polygon where the line is expected to be
        minX = Lpoly[:,:,0].min()
        maxX = Lpoly[:,:,0].max()
        minY = Lpoly[:,:,1].min()
        maxY = Lpoly[:,:,1].max()
        mask = np.zeros(Oimg.shape, dtype=np.uint8)
        cv2.fillConvexPoly(mask, Lpoly, (255,255,255))
        bRes = Oimg[minY:maxY, minX:maxX]
        bMsk = mask[minY:maxY, minX:maxX]
        _, bImg = cv2.threshold(bRes, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        _,cols = bImg.shape
        #--- remove black halo around the image
        bImg[bMsk[:,:]==0] = 255
        Cs= np.cumsum(abs(bImg-255), axis=0)
        maxPoints=np.argmax(Cs, axis=0)
        points = np.zeros((cols,2), dtype='int')
        #--- gen a 2D list of points
        for i,j in enumerate(maxPoints):
            points[i,:] = [i,j]
        #--- remove points at post 0, those are very probable to be blank columns
        points2D = points[points[:,1]>0]
        if (points2D.size <= 15):
            #--- there is no real line
            return (False, [[0,0]])
        if approx_alg == 'optimal':
            #--- take only 100 points to build the baseline 
            if points2D.shape[0] > max_vertex:
                points2D = points2D[np.linspace(0,
                                                points2D.shape[0]-1,
                                                max_vertex,
                                                dtype=np.int)]
            (approxError, approxLin) = pa.poly_approx(points2D,
                                                  num_segments,
                                                  pa.one_axis_delta)
        elif approx_alg == 'trace':
            approxLin = pa.norm_trace(points2D, num_segments)
        else:
            approxLin = points2D
        approxLin[:,0] = approxLin[:,0] + minX
        approxLin[:,1] = approxLin[:,1] + minY
        return (True, approxLin)

    def _xml_list_generator(self, val_xml_dir):
        img_paths = []
        img_paths.extend(glob.glob(val_xml_dir + '/*.' + 'xml'))
        img_ids = [os.path.splitext(os.path.basename(x))[0] for x in img_paths]
        img_ids.sort()

        xml_paths = []
        for ids in img_ids:
            ids_ext = ids+'.xml'
            dir_xml = os.path.join(val_xml_dir, ids_ext)
            xml_paths.append(dir_xml)

        return xml_paths


