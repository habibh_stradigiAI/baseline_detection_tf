from data_fetcher import fetch_X_y
from estimator import BaselineDetector
import cv2

"""
Before running the code, please create the main data directory using the following structure:

data
    ├── train
    │   ├── IMG_0.png
    │   ├── IMG_1.png
    │   ├── ...
    │   └── page
    │       ├── IMG_0.xml
    │       ├── IMG_1.xml
    │       └── ...
    ├── val
    │   ├── IMG_0.png
    │   ├── IMG_1.png
    │   ├── ...
    │   └── page
    │       ├── IMG_0.xml
    │       ├── IMG_1.xml
    │       └── ...
    └── test
        ├── IMG_0.png
        ├── IMG_1.png
        └── ...
"""

# Fetch the input and labels for training, validation and inference
X_train, y_train, X_valid, y_valid, X_infer = fetch_X_y(data_dir='data')

# Create the BaselineDetector
detector = BaselineDetector(output_dir='bestModel', 
                            batch_size=8, 
                            num_epochs=1000,
                            logs_dir='logs', 
                            early_stopping=1000,
                            learning_rate=0.001
                            )

# Run the training and save the best model in output_dir
detector.fit([X_train, X_valid], [y_train, y_valid])
# detector.partial_fit([X_train, X_valid], [y_train, y_valid])

# Run the inference using the saved model
all_baselines = detector.predict(X_infer)

# Just for debuging save the output images to 'work/pred'
iii = 0
for i in range(len(X_infer)):
    org_img = X_infer[i]
    baselines = all_baselines[i]

    test_img = cv2.cvtColor(org_img, cv2.COLOR_GRAY2RGB)
    # Scale the baselines to the original size of the image
    for bl in baselines:
        cv2.polylines(test_img, [bl], False, [0,0,255], 4)

    cv2.imwrite('work/pred/' + str(iii) + '.png', test_img)
    iii += 1