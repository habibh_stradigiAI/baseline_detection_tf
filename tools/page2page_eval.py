from __future__ import print_function
from __future__ import division
from builtins import range

import sys
import os

import numpy as np
import subprocess
import tempfile
from collections import OrderedDict

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../")

def compute_metrics(hyp, target):
    """
    this functon computes the precision, recall and f1-score
    """
    num_samples = len(target)
    metrics = {}
    # --- force dict to appear in the same order always
    summary = OrderedDict()
    metrics.update(
        {
            "p_bl": np.empty(num_samples, dtype=np.float),
            "r_bl": np.empty(num_samples, dtype=np.float),
            "f1_bl": np.empty(num_samples, dtype=np.float),
        }
    )

    # --- sufix must be added because for some extrange reason
    # --- Transkribus tool need it
    t_fd, t_path = tempfile.mkstemp(suffix=".lst")
    h_fd, h_path = tempfile.mkstemp(suffix=".lst")
    try:
        with os.fdopen(t_fd, "w") as tmp:
            tmp.write("\n".join(target))
        with os.fdopen(h_fd, "w") as tmp:
            tmp.write("\n".join(hyp))
        evaltool = os.path.dirname(__file__) + "/baselineEvaluator.jar"
        cmd = subprocess.Popen(
            ["java", "-jar", evaltool, "-no_s", t_path, h_path],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        bl_results, _err = cmd.communicate()
    finally:
        os.remove(t_path)
        os.remove(h_path)

    bl_results = bl_results.decode().split("\n")
    for i in range(num_samples):
        res = bl_results[17 + i].split(",")
        metrics["p_bl"][i] = float(res[0])
        metrics["r_bl"][i] = float(res[1])
        metrics["f1_bl"][i] = float(res[2])
    summary["p_bl"] = bl_results[-6]
    summary["r_bl"] = bl_results[-5]
    summary["f1_bl"] = bl_results[-4]

    avg_presicion = float(bl_results[-6].split(':')[1][1:])
    avg_recall = float(bl_results[-5].split(':')[1][1:])
    F1_score = float(bl_results[-4].split(':')[1][1:])

    # --- return averages only
    return avg_presicion, avg_recall, F1_score

