# Baseline Detection

This model uses a Conditional Generative Adversarial Network to detect the baselines of a document image. This is an improved version of P2PaLA toolkit (originally in PyTorch) provided in the following link:

https://github.com/lquirosd/P2PaLA

# Data Structure

Before running the code, please create the main data directory using the following structure:

```
data
    ├── train
    │   ├── IMG_0.png
    │   ├── IMG_1.png
    │   ├── ...
    │   └── page
    │       ├── IMG_0.xml
    │       ├── IMG_1.xml
    │       └── ...
    ├── val
    │   ├── IMG_0.png
    │   ├── IMG_1.png
    │   ├── ...
    │   └── page
    │       ├── IMG_0.xml
    │       ├── IMG_1.xml
    │       └── ...
    └── test
        ├── IMG_0.png
        ├── IMG_1.png
        └── ...
```

# How to Run

In order to try this code, you only need to run the example file using the following bash script:

```
bash run.sh
```


