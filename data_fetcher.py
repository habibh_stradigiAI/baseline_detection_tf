import os
import cv2
import numpy as np
import glob

from tools.xmlPAGE import pageData

def fetch_X_y(data_dir='data'):
    """ 
    This function is used to generate inputs and masks for the Baseline detection model. 

    Examples
    --------
    X_train, y_train, X_valid, y_valid, X_infer = fetch_X_y(data_dir='data')
    detector = BaselineDetector()
    detector.fit([X_train, X_valid], [y_train, y_valid])

    Parameters
    ----------
    data_dir : str
        This is the main directory where the data is stored. The folder structure of this directory 
        must be like this (note that 'page' is a folder containing all ground-truth information as
        xml files with the same name as its corresponding input image):

        data
        ├── train
        │   ├── IMG_0.png
        │   ├── IMG_1.png
        │   ├── ...
        │   └── page
        │       ├── IMG_0.xml
        │       ├── IMG_1.xml
        │       └── ...
        ├── val
        │   ├── IMG_0.png
        │   ├── IMG_1.png
        │   ├── ...
        │   └── page
        │       ├── IMG_0.xml
        │       ├── IMG_1.xml
        │       └── ...
        └── test
            ├── IMG_0.png
            ├── IMG_1.png
            └── ...

    Returns
    -------
    train_imgs : array-like
        Training images.
    train_labels : array-like
        Ground-truth masks for training.
    valid_imgs : array-like
        Validation images.
    valid_labels : array-like
        Ground-truth masks for validation.
    test_imgs : array-like
        Inference images.
    """

    train_data = _build_input_images('train', data_dir)
    valid_data = _build_input_images('val', data_dir)
    test_data = _build_input_images('test', data_dir)

    train_imgs = np.array([i[0] for i in train_data])
    train_labels = np.array([i[1] for i in train_data])

    valid_imgs = np.array([i[0] for i in valid_data])
    valid_labels = np.array([i[1] for i in valid_data])

    test_imgs = np.array([i[0] for i in test_data])
    # test_labels = np.array([i[1] for i in test_data])

    return train_imgs, train_labels, valid_imgs, valid_labels, test_imgs


def _build_input_images(split, data_dir):
    """ 
    This function creates a list of images and their corresponding ground-truth masks 

    Parameters
    ----------
    split : str
        One of the following options: 'train', 'val', or 'test'. 
    data_dir : str
        This is the main directory where the data is stored (please see the structure of data in above function)

    Returns
    -------
    processed_data : array-like
        A list of input images and their corresponding ground-truth masks as follows:
        processed_data = [(img0, msk0), (img1, msk1), ... ]
        Note that the input images may have different dimentions but the masks will have 
        the dimention equal to the input of the model (default to [1024, 768])
    """

    imgs_dir = os.path.join(data_dir, split)

    formats = ['tif','tiff', 'png', 'jpg', 'jpeg', 'JPG','bmp']
    fnames_in = []
    for ext in formats:
        fnames_in.extend(glob.glob(imgs_dir + '/*.' + ext))

    processed_data = []
    for img_path in fnames_in:
        processed_data.append(_processData(img_path,
                                            out_size=(1024, 768),
                                            classes=2,
                                            line_width=8,
                                            line_color=255,
                                            ext_mode='C',
                                            num_chanels=1))

    return processed_data

def _processData(img_path, 
                 out_size, 
                 classes,
                 line_width, 
                 line_color, 
                 ext_mode, 
                 num_chanels,
                 expand_gray=True):
    """
    Load and preprocess images and generate mask images from PAGE files 
    """
    img_id = os.path.splitext(os.path.basename(img_path))[0]
    img_dir = os.path.dirname(img_path)

    # For grey scale input
    if num_chanels == 1:
        res_img = cv2.imread(img_path,0)
    # For RGB input
    else:
        res_img = cv2.imread(img_path)

    if num_chanels == 1 and expand_gray:
        res_img = np.expand_dims(res_img, axis=-1)

    # Chack if the page folder exists for the test set
    # if not, it is inference only
    if os.path.isdir(os.path.join(img_dir, 'page')):

        #--- get label
        if (os.path.isfile(img_dir + '/page/' + img_id + '.xml')):
            xml_path = img_dir + '/page/' + img_id + '.xml'
        else:
            print('No xml found for file {}'.format(img_path))
            raise Exception("Execution stop due Critical Errors")

        gt_data = pageData(xml_path)
        gt_data.parse()

        #--- build lines mask
        label = gt_data.build_baseline_mask(out_size,line_color,line_width)

        # add an extra dimention for gray images
        if expand_gray: 
            label = np.expand_dims(label, axis=-1)

        return (res_img, label)
    else:
        return (res_img, [])